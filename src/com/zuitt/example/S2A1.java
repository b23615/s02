package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class S2A1 {
    public static void main(String[] args){
        System.out.println("Input year to be checked if a leap year.");

//        ArrayList<Integer> years = new ArrayList<>(Arrays.asList(1985, 1988, 1980, 400, 100, 800, 200)); //additional value to test case.

//        for (int year : years) {
//            if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0){
//                System.out.println(year + " is a leap year");
//            }
//            else {
//                System.out.println(year + " is NOT a leap year");
//            }
//        }

        Scanner numberScanner = new Scanner(System.in);
        int year = numberScanner.nextInt();

        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0){
            System.out.println(year + " is a leap year");
        }
        else {
            System.out.println(year + " is NOT a leap year");
        }
    }
}
