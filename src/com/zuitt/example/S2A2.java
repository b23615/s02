package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class S2A2 {
    public static void main(String[] args){
        int[] primeNumbers = new int[5];

        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        System.out.println(Arrays.toString(primeNumbers));
        System.out.println("The first prime number is: " + primeNumbers[0]);
        System.out.println("The first prime number is: " + primeNumbers[1]);
        System.out.println("The first prime number is: " + primeNumbers[2]);
        System.out.println("The first prime number is: " + primeNumbers[3]);
        System.out.println("The first prime number is: " + primeNumbers[4]);

        ArrayList<String> students = new ArrayList<String>();
        students.add("John");
        students.add("Jane");
        students.add("Chloe");
        students.add("Zoey");

        System.out.println("My friends are: " + students);

        HashMap<String, Integer> inventory = new HashMap<String, Integer>();
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 28);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);
    }
}
